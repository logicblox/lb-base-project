Here's what's in the demo application:

* Sample hierarchies 
  (sources in `src/hierarchy/hierarchy/*.logic`):
    * `calendar.logic`
    * `location.logic`
    * `product.logic`
* Delimited file services that expose the above hierarchy under `/hierarchy/*`
  (sources in `src/hierarchy_services/hierarchy_services/*.logic`):
    * `calendar.logic`
    * `location.logic`
    * `product.logic`
* Some example sales measures (sources in `src/sales/sales/sales.logic`)
* Delimited file services that expose the above sales measures under `/sales/*`
  (sources in `src/sales_services/sales_services/sales.logic`).
* A sample bloxweb protobuf JSON service that exposes location data
  (sources in `src/json_services`):
    * `protocols/location.proto`: defines the Request/Response protobuf message
    * `json_services/location_json.logic`: implements the location services
    * `json_services/service_config.logic`: exposes location service to `/json/location`
    as well as in an authenticated way under `/json/location_authenticated` (for demo purposes only).
* A sample batch script (in `test/test.batch`) that is executed by the `./run` script to import some sample
  data into the workspace.
* A minimal web UI with authentication (username: user1, password: password) that calls the 
  `/json/location_authenticated` and measure service on load.
  Note that the user is automatically redirected to the login page _only_ when a web service is called
  that requires authentication. To disable having to login, simply disable calls to authenticated services.
