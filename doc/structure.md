This project contains the following directories:

* `conf`: this directory contains nginx configuration files for local testing (with `./run`)
* `doc`: documentation for the project
* `src`: contains the LogiQL source code, one module per directory. Directories that end 
  with `_services` are modules that expose either custom LogiQL services or delimited file
  services. Each module directory contains a `.project` file with the same name, and a
  directory with the same name containing the actual `.logic` and `.proto` files.
* `test`: contains unit test and test-related files
* `www`: contains static web resources that form the front-end of the application, typically 
  consisting of  HTML, CSS, images and JavaScript files.

The root directory contains the following files:

* `.hgignore`: is a file for mercurial that hints at what files may or may not have to be checked in.
* `README.md`: describes the project and how to develop and run it
* `config.py`: contains the lb-config specification for building the project (Python code)
* `default.nix`: specifies all dependencies of the project, including the platform version and is used
  to setup the development environment using `nix-build --run-env` from the project's root directory.
* `run`: is a script that is used for running the application locally, it will build the project, deploy
  it to the `run_workspace` workspace, start bloxweb services and then run nginx serving the web UI on
  `http://localhost:8086`. The `run` script can also be run with `--keep` for it to not rebuild the workspace.
