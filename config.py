#! /usr/bin/env python

from lbconfig.api import *

lbconfig_package('app', version='0.1', default_targets=['lb-libraries'])

depends_on(
    logicblox_dep,
    bloxweb_dep,
    bloxweb_measure_service='$(BLOXWEB_MEASURE_SERVICE_HOME)'
)

libraries = [
    'hierarchy',
    'sales', 
    'hierarchy_services',
    'sales_services',
    'measure_services',
    'json_services',
]

for lib in libraries:
    lb_library(
       name = lib,
       srcdir = 'src',
       deps = { 'bloxweb' : '$(bloxweb)' }
    )

rule('start-services', '', [
  '$(bloxweb)/bin/bloxweb install-jar $(bloxweb_measure_service)/lib/java/BloxWebMeasureService.jar',
  '$(bloxweb)/bin/bloxweb start-services',
], True)


check_lb_workspace(name='test_workspace', libraries=libraries)
# Make sure  services are installed for this workspace
rule('check-ws-test_workspace', 'start-services', [], True)

# Workspace for running for local testing
check_lb_workspace(name='run_workspace', libraries=libraries)
# Make sure services are installed for this workspace
rule('check-ws-run_workspace', 'start-services', [], True)

check_program('test/all.py', workspaces=['test_workspace'])
