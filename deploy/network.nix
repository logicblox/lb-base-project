{
  machine =
    {config, pkgs, ...}:
    {
      require = [ 
        <lbdevops/nixos/installer.nix>
        <lbdevops/nixos/lb-module.nix>
        <lbdevops/nixos/nginx.nix> 
        <lbdevops/nixos/client-specific/predictix-ssl.nix>
      ] ;

      services.logicblox.enable = true;
      services.logicblox.logicblox = builtins.storePath <logicblox>;
      services.logicblox.bloxweb = builtins.storePath <bloxweb>;

      logicblox.application.installer = builtins.storePath <installer>;
    };
}
