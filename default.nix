{ nixpkgs ? <nixpkgs>
#, lb_version ? "3.10.4"
, lb_version ? "4.0.1"
}:
with import <config/lib> { inherit nixpkgs; };
let
  pkgs = import nixpkgs {};
  stdenv = pkgs.stdenv;
  platform = builtins.getAttr lb_version releases.platform;
in
with platform;
{
  build = buildLB rec {
    name = "myapp";
    src = ./.;
    buildInputs = [
      # Core LB components
      lb-config
      logicblox
      bloxweb
      bloxweb-measure-service
      # Auxiliarily tools
      pkgs.python26Packages.readline # for lb interactive
      pkgs.nginx
    ];
    configureScript = "lb-config";
  };
}
