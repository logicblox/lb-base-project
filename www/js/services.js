"use strict";

var angular = window.angular;

var services = angular.module('app.services', []);

services.config(['$httpProvider', function($httpProvider) {
    // Set the LB specific headers for accessing services for all POST
    // requests
    $httpProvider.defaults.headers.post["Accept"]="application/json";
}]);

services.factory('TestService', ['$http', '$q', function($http, $q, cache) {
    return {
        getLocations: function(callback) {
            var data = {};
            $http.post("/bloxweb/json/location_authenticated", data).success(callback);
        },
        getProductsFromMeasureService: function(callback) {
            var query = {
                "kind": "QUERY",
                "query_request": {
                    "report_name": "attr",
                    "return_rows": false,
                    "measure": {
                        "kind": "ATTRIBUTE",
                        "attribute": {
                            "qualified_level": {
                                "dimension": "Product",
                                "level": "sku"
                            },
                            "attribute": "id"
                        }
                    }
                }
            };
            $http.post("/bloxweb/measure", query).success(function(result) {
                callback(result.report_column[1].string_column.value);
            });
        }
    };
}]);

services.factory('UserService', ['$http', 'authService', function($http, authService) {
    return {
        login: function(username, password, callback) {
            var data = {
                realm: "location-realm",
                username: username,
                password: password
            };
            var success = function(response) {
                if (response.success === true) {
                    authService.loginConfirmed();
                    callback && callback(response);
                } else {
                    callback && callback(response);
                }
            };
            $http.post("/bloxweb/login", data).success(success);
        },
        logout: function(callback) {
            var data = {
                realm: "location-realm",
                logout: true
            };

            $http.post("/bloxweb/login", data).success(function(response) {
                if (response.success === true && callback !== undefined) {
                    callback(response);
                }
            });
        }
    };
}]);
