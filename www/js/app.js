'use strict';

// Declare app level module which depends on filters, and services
angular.module('app', ['app.services', 'http-auth-interceptor'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: Home});
        $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: Login});

        $routeProvider.otherwise({redirectTo: '/home'});
    }])
    .run(function($rootScope, $location, UserService) {
        $rootScope.loggedUser = null;

        $rootScope.logout = function() {
            UserService.logout(function(result) {
                if (result) {
                    $rootScope.loggedUser = null;
                    $location.path('/login');
                }
            });
        };

        // To disable required authentication, comment out the following statement
        $rootScope.$on('event:auth-loginRequired', function() {
            console.log("YEAHHHH");
            if ($location.path() !== "/login") {
                $location.path( "/login" );
            }
        });
    });
