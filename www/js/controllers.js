'use strict';

function Home($scope, $location, TestService) {
    $scope.requireLogin = true;
    $scope.locations = [];
    $scope.products = [];

    $scope.loadData = function() {
        TestService.getLocations(function(result) {
            $scope.locations = result.result;
        });
        TestService.getProductsFromMeasureService(function(result) {
            $scope.products = result;
        });
    };
}

function Login($scope, $rootScope, $location, UserService) {
    $scope.username = "";
    $scope.password = "";
    $scope.errorMessage = "";

    $scope.login = function() {
        UserService.login($scope.username, $scope.password, function(result) {
          if(result.success) {
              $rootScope.loggedUser = $scope.username;
              $location.path("/home");
          } else {
              $scope.errorMessage = "Invalid username/password";
          }
        });
    };
}


function NavList($scope, $location) {
    $scope.navCls = function (page) {
        var currentRoute = $location.path().substring(1) || 'home';
        return page === currentRoute ? 'active' : '';
    };
}
