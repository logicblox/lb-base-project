NOT WORKING ON 4.0 (use 3.10 branch for LB 3.10)
================================================

IMPORTANT: THIS BASE PROJECT CURRENTLY ONLY SUPPORTS 3.10, WHICH
CAN BE FOUND IN THE 3.10 BRANCH. AS SOON AS 4.0 IMPROVES ITS
DELIMITED FILE SUPPORT A 4.0 VERSION WILL ALSO BE RELEASED. 

Preparation (once per system)
=============================

1. [Install Nix](https://sites.google.com/a/logicblox.com/platform/teams/deploymentblox/installing-nix-on-ubuntu?pli=1)
2. Add the nix-lb-dev channel and install the `nix-resources` package:

        $ nix-channel --add http://builder.logicblox.com/jobset/nix-lb-dev/master/channel/latest nix-lb-dev
        $ nix-channel --update nix-lb-dev
        $ nix-env -i nix-resources nixops --option binary-caches http://builder.logicblox.com

3. Add the following to your `~/.bash_profile` (or whatever your shell's startup script is) and execute it in your current
   shell to activate it immediately:

        source $HOME/.nix-profile/etc/profile.d/nix-resources.sh

Developing/running this application
===================================

To run and develop this application you do not have to have LogicBlox installed, the right version will be
install automatically using Nix.

To enter the shell with all required software available:

    $ nix-shell -A build --pure

If this fails with warnings about not being able to download the JDK, you can follow the detailed instructions
for downloading the JDK among the warnings, or use the following:

    $ nix-shell -A build --pure --option binary-caches http://builder.logicblox.com

Alternatively, to use an LB version different than the default, you can pass it as an argument:

    $ nix-shell -A build --pure --argstr lb_version 4.0.0

This will spawn a sub-shell. You're now ready to go. To exit the shell, press Ctrl-D or type `exit`.

To build the application simply run:

    $ lb-config
    $ make

To run tests:

    $ make check

To run the application and access it via the web, run:

    $ ./run

Or to start nginx without rebuilding the workspace:

    $ ./run --keep

More documentation can be found in the `doc/` directory.

Current [build status](https://builds.logicblox.com/logicblox/lb-base-project-3103/): ![Build status image](https://builds.logicblox.com/logicblox/lb-base-project-3103/status).
