#! /usr/bin/env python

import sys
import os
import unittest
import json

sys.path.insert(0, '%s/lib/python' % os.environ.get('LOGICBLOX_HOME'))
sys.path.insert(0, '%s/lib/python' % os.environ.get('BLOXWEB_HOME'))

import bloxweb.testcase
import bloxweb.service
import bloxweb.admin

class MeasureServicesTestCase(bloxweb.testcase.PrototypeWorkspaceTestCase):
    prototype = "test_workspace"

    def add_product_data(self):
        self.delimClient.post("""
            Sunkist OJ|Juice|Food|Sunkist
            Tropicana OJ|Juice|Food|Tropicana
            FL Best OJ|Juice|Food|Florida's Best
            Kashi Bar|Snacks|Food|Kashi
            Kashi Cereal|Cereal|Food|Kashi
            NV Bar|Snacks|Food|Nature's Valley
            Cheerios|Cereal|Food|General Mills
            Wheaties|Cereal|Food|General Mills""")

    def prepare(self):
        self.delimClient = bloxweb.service.DelimClient("localhost", 8080, "/hierarchy/product")
        self.client = bloxweb.service.Client("localhost", 8080, "/measure")
        self.add_product_data()

    def test_measure(self):
        self.prepare()
        json_s = json.dumps({
            "kind": "QUERY",
            "query_request": {
                "report_name": "attr",
                "return_rows": False,
                "measure": {
                    "kind": "ATTRIBUTE",
                    "attribute": {
                        "qualified_level": {
                            "dimension": "Product",
                            "level": "sku"
                        },
                        "attribute": "id"
                    }
                }
            }
        })
        res_json = self.client.call_json(json_s)
        res_dict = json.loads(res_json)
        values = res_dict["report_column"][1]["string_column"]["value"]
        self.assertEquals(len(values), 8)
        self.assertTrue("Sunkist OJ" in values)


def suite(args):
    suite = unittest.TestSuite()
    if (len(args) == 1):
        suite.addTest(unittest.makeSuite(MeasureServicesTestCase))
    else:
        suite.addTest(HierarchyServiceTestCase(args[1]))
    return suite

if __name__ == '__main__':
    result = unittest.TextTestRunner(verbosity=2).run(suite(sys.argv))
    sys.exit(not result.wasSuccessful())
