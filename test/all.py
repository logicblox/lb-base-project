#!/usr/bin/env python

import unittest, sys

import test_sales_services
import test_hierarchy_services
import test_json_services
import test_measure_services

tests = [
   test_hierarchy_services,
   test_sales_services,
   test_json_services,
   test_measure_services
]

if __name__ == '__main__':
    suite = unittest.TestSuite()
    for test in tests:
        suite.addTest(test.suite(sys.argv))
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    sys.exit(not result.wasSuccessful())
