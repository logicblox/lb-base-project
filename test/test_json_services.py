#! /usr/bin/env python

import sys
import os
import unittest

import json
import urllib2

sys.path.insert(0, '%s/lib/python' % os.environ.get('LOGICBLOX_HOME'))
sys.path.insert(0, '%s/lib/python' % os.environ.get('BLOXWEB_HOME'))

import bloxweb.testcase
import bloxweb.service
import bloxweb.credentials
import bloxweb.admin

class JSONServiceTestCase(bloxweb.testcase.WorkspaceTestCase):
    workspaces=['login', 'test_workspace']

    def add_location_data(self):
        self.delimClient.post("""
                AtlantaStore|Atlanta|GN|North America
                NewYorkStore|New York|NY|North America
                """)

    def prepare(self):
        self.delimClient = bloxweb.service.DelimClient("localhost", 8080, "/hierarchy/location")
        self.client = bloxweb.service.Client("localhost", 8080, "/json/location")
        self.add_location_data()

    def json_call(self, url, data):
        jsons = json.dumps(data)
        req = urllib2.Request(url, jsons, {'Content-Type': 'application/json',
                                           'Accept': "application/json"})
        f = urllib2.urlopen(req)
        response = f.read()
        f.close()
        return json.loads(response)

    def test_location(self):
        self.prepare()
        req = self.client.dynamic_request()
        response = self.client.dynamic_call(req)
        self.assertEqual(2, len(response.result))
        for result in response.result:
            self.assertTrue(result.store in ["AtlantaStore", "NewYorkStore"])
            self.assertTrue(result.city in ["New York", "Atlanta"])
            self.assertEqual("North America", result.region)

    def test_location_authenticated(self):
        self.prepare()

        # Create user
        credentials_client = bloxweb.credentials.Client()
        credentials_client.set_password("user1", "password", create=True)

        # Login
        login_client = bloxweb.service.Client("localhost", 8080, "/login")
        loginResponse = login_client.login("user1", "password", "location-realm")
        self.assertEquals(loginResponse['success'], True)

        client = bloxweb.service.Client("localhost", 8080, "/json/location_authenticated")
        # Make sure our client shares its cookies with the login_clinet
        client.jar = login_client.jar

        req = client.dynamic_request()
        response = client.dynamic_call(req)
        self.assertEqual(2, len(response.result))
        for result in response.result:
            self.assertTrue(result.store in ["AtlantaStore", "NewYorkStore"])
            self.assertTrue(result.city in ["New York", "Atlanta"])
            self.assertEqual("North America", result.region)

    def test_location_json(self):
        self.prepare()
        result_json = self.json_call("http://localhost:8080/json/location", {})
        self.assertEqual(2, len(result_json["result"]))
        for result in result_json["result"]:
            self.assertTrue(result["store"] in ["AtlantaStore", "NewYorkStore"])
            self.assertTrue(result["city"] in ["New York", "Atlanta"])
            self.assertEqual("North America", result["region"])

def suite(args):
    suite = unittest.TestSuite()
    if (len(args) == 1):
        suite.addTest(unittest.makeSuite(JSONServiceTestCase))
    else:
        suite.addTest(JSONServiceTestCase(args[1]))
    return suite

if __name__ == '__main__':
    result = unittest.TextTestRunner(verbosity=2).run(suite(sys.argv))
    sys.exit(not result.wasSuccessful())
