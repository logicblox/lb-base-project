#! /usr/bin/env python

import sys
import os
import unittest

sys.path.insert(0, '%s/lib/python' % os.environ.get('LOGICBLOX_HOME'))
sys.path.insert(0, '%s/lib/python' % os.environ.get('BLOXWEB_HOME'))

import bloxweb.testcase
import bloxweb.service
import bloxweb.admin

class SalesServiceTestCase(bloxweb.testcase.PrototypeWorkspaceTestCase):
    prototype = "test_workspace"

    def get_client(self, path):
        return bloxweb.service.DelimClient("localhost", 8080, path)

    def roundtrip_check(self, url, data):
        client = self.get_client(url)
        client.post(data)
        self.assertDelimEqual(client.get(), data)

    def test_monthly_roundtrip(self):
        self.roundtrip_check("/sales/monthly", """
                table|AtlantaStore|1|12
                table|NewYorkStore|1|8
        """)

    def test_daily_roundtrip(self):
        self.roundtrip_check("/sales/daily", """
                table|AtlantaStore|1|12
                table|NewYorkStore|1|8
        """)

def suite(args):
    suite = unittest.TestSuite()
    if (len(args) == 1):
        suite.addTest(unittest.makeSuite(SalesServiceTestCase))
    else:
        suite.addTest(SalesServiceTestCase(args[1]))
    return suite

if __name__ == '__main__':
    result = unittest.TextTestRunner(verbosity=2).run(suite(sys.argv))
    sys.exit(not result.wasSuccessful())
