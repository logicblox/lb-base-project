#! /usr/bin/env python

import sys
import os
import unittest

sys.path.insert(0, '%s/lib/python' % os.environ.get('LOGICBLOX_HOME'))
sys.path.insert(0, '%s/lib/python' % os.environ.get('BLOXWEB_HOME'))

import bloxweb.testcase
import bloxweb.service
import bloxweb.admin

class HierarchyServiceTestCase(bloxweb.testcase.PrototypeWorkspaceTestCase):
    prototype = "test_workspace"

    def get_client(self, path):
        return bloxweb.service.DelimClient("localhost", 8080, path)

    def roundtrip_check(self, url, data):
        client = self.get_client(url)
        client.post(data)
        self.assertDelimEqual(client.get(), data)

    def test_calendar_roundtrip(self):
        self.roundtrip_check("/hierarchy/calendar", """
                DAY|MONTH|YEAR
                1|1|2013
                2|1|2013
        """)

    def test_location_roundtrip(self):
        self.roundtrip_check("/hierarchy/location", """
                STORE|CITY|STATE|REGION
                AtlantaStore|Atlanta|GN|North America
                NewYorkStore|New York|NY|North America
                """)

    def test_product_roundtrip(self):
        self.roundtrip_check("/hierarchy/product", """
                SKU|SUBCLASS|CLASS|BRAND
                Sunkist OJ|Juice|Food|Sunkist
                Tropicana OJ|Juice|Food|Tropicana
                FL Best OJ|Juice|Food|Florida's Best
                Kashi Bar|Snacks|Food|Kashi
                Kashi Cereal|Cereal|Food|Kashi
                NV Bar|Snacks|Food|Nature's Valley
                Cheerios|Cereal|Food|General Mills
                Wheaties|Cereal|Food|General Mills""")

def suite(args):
    suite = unittest.TestSuite()
    if (len(args) == 1):
        suite.addTest(unittest.makeSuite(HierarchyServiceTestCase))
    else:
        suite.addTest(HierarchyServiceTestCase(args[1]))
    return suite

if __name__ == '__main__':
    result = unittest.TextTestRunner(verbosity=2).run(suite(sys.argv))
    sys.exit(not result.wasSuccessful())
